package com.c2t.hf.factory.factory;

public interface Cheese {
	public String toString();
}
