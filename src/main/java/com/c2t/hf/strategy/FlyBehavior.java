package com.c2t.hf.strategy;

public interface FlyBehavior {
	public void fly();
}
