package com.c2t.dp.singleton;

public class Loggers {
	
	private static Loggers log = new Loggers();
	
	private Loggers(){
	}
	
	public static Loggers getLogger(){
		return log;
	}
}
