package com.c2t.dp.ducks;

public interface Duck {
	public void quack();
	public void fly();
}
