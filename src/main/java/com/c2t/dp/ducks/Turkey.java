package com.c2t.dp.ducks;

public interface Turkey {
	public void gobble();
	public void fly();
}
